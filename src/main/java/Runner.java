import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.io.IOException;

/**
 * Created by brand_000 on 10/3/14.
 */
public class Runner {
    public void basicRun() throws Exception{
        Tweets tweets = new Tweets("data/ct.csv", "normal");

        Extractor extractor = new Extractor(tweets, true);
        extractor.extract();

        Baseline b = new Baseline(tweets, "1");
        Combined c = new Combined(tweets);
    }

    public void turkRun() throws Exception{
        Tweets tweets = new Tweets("data/sandySectNew.csv", "turk");

        Extractor extractor = new Extractor(tweets, true);
        extractor.turkExtract();
    }

    public void check1() throws Exception{
        Tweets tweets = new Tweets("data/turk.csv", "turk");
    }

    public void checkOldNew() throws IOException {
        Tweets turk1 = new Tweets("data/sandyNew.csv", "turk");
        Tweets turk2 = new Tweets("data/sandyOld.csv", "turk");

        MatchChecker checker = new MatchChecker();

        checker.checkOldNew(turk1, turk2);
    }

    public void test() throws IOException{
        Tweets turk = new Tweets("data/new/consitent_file/sandy_q1_consist.csv", "3");
        Baseline b = new Baseline(turk, "2");
    }

    public void organize(int num, String kind,String positive, String negative) throws Exception{
        Tweets tweets = new Tweets("data/sandySectNew.csv", "turk");

        // Extractor extractor = new Extractor(tweets, true);
        // extractor.turkExtract();
//        System.out.print("Enter num of informational times: ");
//        Scanner sr = new Scanner(System.in);
//        int num = sr.nextInt();
//        System.out.println(num);
//        System.out.print("Enter what to look for: ");
//        String kind = sr.next().toLowerCase();
//        System.out.println(kind);
//        System.out.print("Enter positive: ");
//        String positive= sr.next().toUpperCase();
//        System.out.println(positive);
//        System.out.print("Enter neg: ");
//        String negative=sr.next().toUpperCase();
//        System.out.println(negative);
        tweets.getNewdata(num,kind,positive,negative);
    }

}
