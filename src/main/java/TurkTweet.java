/**
 * Created by brand_000 on 10/3/14.
 * tweet, id, q1, q2, q3, q4, q5, q6, q7, q8, q9, geo
 *
 * q6 = YesInfo or NoInfo
 */
public class TurkTweet {
    int length;
    String id, timestamp, tweet, q1, q2, q3, q4, q5, q6, q7, q8, q9, geo, classification, original;
    public TurkTweet(String _id,
                     String _timestamp,
                     String _tweet,
                     String _q1,
                     String _q2,
                     String _q3,
                     String _q4,
                     String _q5,
                     String _q6,
                     String _q7,
                     String _q8,
                     String _q9,
                     String _geo,
                     String _original){
        id = _id;
        timestamp = _timestamp;
        tweet = _tweet;
        q1 = _q1;
        q2 = _q2;
        q3 = _q3;
        q4 = _q4;
        q5 = _q5;
        q6 = _q6;
        //System.out.println("Question 6: " + q6);
        classification = (q6.equals("YesInfo")) ? "informational" : "conversational";
        q7 = _q7;
        q8 = _q8;
        q9 = _q9;
        geo = _geo;
        original = _original;
    }
    public String getOriginal()
    {
        return original;
    }
    public int getAnswer(String type, String pos, String neg)
    {
        if(type.equals("relevance"))
        {
            if(q1.toUpperCase().equals(pos))
                return 0;
            else if(q1.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("support"))
        {
            if(q2.toUpperCase().equals(pos))
                return 0;
            else if(q2.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("emotion"))
        {
            if(q3.toUpperCase().equals(pos))
                return 0;
            else if(q3.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("feeling"))
        {
            if(q4.toUpperCase().equals(pos))
                return 0;
            else if(q4.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("humor"))
        {
            if(q5.toUpperCase().equals(pos))
                return 0;
            else if(q5.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("info"))
        {
            if(q6.toUpperCase().equals(pos))
                return 0;
            else if(q6.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("useful"))
        {
            if(q7.toUpperCase().equals(pos))
                return 0;
            else if(q7.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("geo"))
        {
            if(q8.toUpperCase().equals(pos))
                return 0;
            else if(q8.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else if(type.equals("trustworthy")) {
            if (q9.toUpperCase().equals(pos))
                return 0;
            else if (q9.toUpperCase().equals(neg))
                return 1;
            else
                return 2;
        }
        else
            return 3;



    }
}
