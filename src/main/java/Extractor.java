import java.io.*;
import java.net.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by brandon on 2/17/14.
 * Features:
 *
 *
 **/
//phone number -- regex, match with keywords
public class Extractor{
    String newLine;
    boolean format; //false --> no 0s, true --> yes 0s
    Tweets tweets;

    public Extractor(Tweets twts, boolean form) throws IOException{
        format = form;
        tweets = twts;
    }

    String header_ = "@relation informationVSconversation\n\n";
    boolean first = true;
    int attribute_count = 0;

    public void addAttribute(String n, String name, String type){
        if(first){
            header_ += "@attribute "
                    +  name + " "
                    +  type + "\n";
        }
        if(format == true){
            if(attribute_count == 0){
                newLine += n;
                attribute_count++;
                return;
            }else{
                newLine += "," + n;
                attribute_count++;
                return;
            }
        }
        else {
            if(!n.equals("0")){
                newLine += "," + attribute_count + " " + n;
                attribute_count++;
            }
            else{
                attribute_count++;
            }
        }

    }

    public String str(int i){
        return Integer.toString(i);
    }

    public String str(double i){
        return Double.toString(i);
    }

    static boolean isUpperCase(char s){
        return s >= 'A' && s <= 'Z';
    }

    static boolean isLowerCase(char s){
        return s >= 'a' && s <= 'z';
    }

    public static String lengthen(String getUrl) throws Exception{
        while(getUrl.contains("http://t.co") || getUrl.contains("http://bit.ly") || getUrl.contains("http://goo.gl") || getUrl.contains("http://tinyurl.com")){
            URL shortened = new URL(getUrl.toString());
            HttpURLConnection urlConnection = (HttpURLConnection) shortened.openConnection();
            urlConnection.setInstanceFollowRedirects(false);
            getUrl = urlConnection.getHeaderField("location");
            if(getUrl == null)
                return "invalid";
        }
        return getUrl;

    }

    public void extract() throws Exception{

        Sentence sentence = new Sentence();
        newLine = "";
        File file = new File("out.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));

        File urlFile = new File("urls");
        Writer urlWriter = new BufferedWriter(new FileWriter(urlFile));
        //-------------------------------------------------------------------
        String e = ":-) :) :o) :] :3 :c) :> =] 8) =) :} :^) :'-) :')" +
                ":-D :D 8-D 8D x-D xD X-D XD =-D =D =-3 =3 B^D :-)) |;-) |-O <3 >:[ :-( :( :-c :c :-< :< :-[ :[ :{ :'-( :'( QQ" +
                ":-|| :@ >:( D:< D: D8 D; D= DX v.v D-': >:) >;) >:-)" +
                ">:\\ >:/ :-/ :-. :/ :\\ =/ =\\ :L =L :S >.< }:-) }:) 3:-) 3:)";
        String[] emoticons = e.split("\\s");
        String slangWords = "lol lolz rofl lolol lmao lmsao ty tyvm omg lmao sux wtf 2fb omfg ya tbh tf yolo smh xox dont " +
                "yall til dnt ";
        String[] cusswords = "fuck shit damn bitch hoe cunt ass nigga gdit screwed screw nig".split("\\s");
        String[] keywords = "Why How Text TXT Call Donate Visit Help learn please update Donate donation volunteer pm donating".split("\\s");
        //-------------------------------------------------------------------
        int tweetcount = 0;


        for(Tweet t : tweets.set){

            // Contains hashtag
            int htCnt = 0;
            if(t.tweet.contains("#")){
                Pattern htPat = Pattern.compile("\\#");
                Matcher htMatch = htPat.matcher(t.tweet);
                while(htMatch.find()){
                    htCnt++;
                }
                addAttribute("1", "containsHashTag", "{0, 1}");
            } else{
                addAttribute("0", "containsHashTag", "{0, 1}");
            }


            // Get sentence parsed tweet
            String[] sentenceParsedTweet = sentence.getSentencesFromString(t.tweet);
            int hasOneWordSentence = 0;

            // Has 1 word sentence
            for(String i : sentenceParsedTweet){
                if(i.split("\\s").length == 1){
                    hasOneWordSentence = 1;

                }
            }
            if(hasOneWordSentence == 1) addAttribute("0", "oneWordSentence", "{0, 1}");
            else addAttribute("1", "oneWordSentence", "{0, 1}");

            // Check for multiple sentences
            if(sentenceParsedTweet.length > 1){
                addAttribute("0", "multipleSentences", "{0, 1}");
            }
            else addAttribute("1", "multipleSentences", "{0, 1}");


            // URL tracking
            Pattern linkPattern = Pattern
                    .compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Pattern pnPattern = Pattern
                    .compile("[0-9]{3}");
            Pattern pnPattern2 = Pattern
                    .compile("^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-‌​9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})$");
            Matcher linkMatch = linkPattern.matcher(t.tweet);

            String urlTweet = t.tweet;
            String url;
            Pattern dateInLinkPattern = Pattern
                    .compile("[0-9]{4}/[0-9]{2}");
            String newsURL = "nyt cnn news huff post time press gov po.st abc ne.ws";

            if(urlTweet.contains("http://t.co") || urlTweet.contains("http://bit.ly") || urlTweet.contains("http://goo.gl") || urlTweet.contains("http://tinyurl.com")){
                boolean goodURL = true;
                int pos = urlTweet.indexOf("http://t.co");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://tinyurl.com");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://bit.ly");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://goog.gl");
                int pos2 = urlTweet.indexOf(" ", pos);
                if(pos2 == -1){
                    urlTweet = urlTweet + " ";
                    pos2 = urlTweet.length()-1; //end of line
                }
                url = lengthen(urlTweet.substring(pos, pos2));
                System.out.println(url);
                urlWriter.write(t.tweet + "\n" + t.classification + "\n" + url + "\n\n");
                Matcher dtMatch = dateInLinkPattern.matcher(url);
                if(dtMatch.find()) goodURL = true;
                for(String i : newsURL.split(" ")){
                    if(url.contains(i)) goodURL = true;
                }
                if(url.split("-").length > 3 || url.split("_").length > 3)
                    goodURL = true;
                String badLinks = "instagr 4sq photo twitter youtube";
                for(String i : badLinks.split(" ")){
                    if(url.contains(i)) goodURL = false;
                }


                if(goodURL)
                    addAttribute("1", "goodURL", "{0,1}");
                else addAttribute("0", "goodURL", "{0,1}");
            }else{
                addAttribute("0", "goodURL", "{0,1}");
            }

            Matcher pnMatch = pnPattern.matcher(t.tweet);
            Matcher pnMatch2 = pnPattern.matcher(t.tweet);
            if(pnMatch.find() || pnMatch2.find()){
                addAttribute("1", "containsPhoneNumber", "{0, 1}");
            } else{
                addAttribute("0", "containsPhoneNumber", "{0, 1}");
            }

            int hasEmoticon = 0;
            for(String i : emoticons){
                if(t.tweet.contains(i)){
                    hasEmoticon = 1;
                    break;
                }
            }

            // Contains emoticon
            if(hasEmoticon == 1){
                addAttribute("1", "containsEmoticon", "{0, 1}");
            } else{
                addAttribute("0", "containsEmoticon", "{0, 1}");
            }

            // Contains retweet
            if(t.tweet.contains("RT")){
                addAttribute("1", "containsRetweet", "{0, 1}");
            } else {
                addAttribute("0", "containsRetweet", "{0, 1}");
            }

            // Contains keyword
            String lowerCaseTweet = t.tweet.toLowerCase();
            String containsKeyWord = "0";
            for(String i : keywords){
                if(t.tweet.contains(i)){
                    containsKeyWord = "1";
                    break;
                }
            }
            addAttribute(containsKeyWord, "containsKeyword", "{0, 1}");

            // Contains cuss word
            String containsCuss = "0";
            for(String i : cusswords){
                if(lowerCaseTweet.contains(i)){
                    containsCuss = "1";
                    break;
                }
            }

            addAttribute(containsCuss, "containsCuss", "{0, 1}");

            // Classification
            addAttribute(t.classification, "classification", "{conversational informational}");

            // Add the end of the data thing
            if(first){
                newLine += "\n";
                writer.write(header_+ "\n@data\n");
                writer.write(newLine);
                first = false;
            } else{
                newLine += "\n";
                System.out.println(t.tweet);
                writer.write(newLine);
            }

            tweetcount++;
            System.out.println(tweetcount);
            System.out.println(newLine);
            attribute_count = 0;
            newLine = "";
        }
        writer.close();
        urlWriter.close();
    }

    public void turkExtract() throws Exception{

        Sentence sentence = new Sentence();
        newLine = "";
        File file = new File("turkoutsectnew.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));

        File urlFile = new File("urls");
        Writer urlWriter = new BufferedWriter(new FileWriter(urlFile));
        //-------------------------------------------------------------------
        String e = ":-) :) :o) :] :3 :c) :> =] 8) =) :} :^) :'-) :')" +
                ":-D :D 8-D 8D x-D xD X-D XD =-D =D =-3 =3 B^D :-)) |;-) |-O <3 >:[ :-( :( :-c :c :-< :< :-[ :[ :{ :'-( :'( QQ" +
                ":-|| :@ >:( D:< D: D8 D; D= DX v.v D-': >:) >;) >:-)" +
                ">:\\ >:/ :-/ :-. :/ :\\ =/ =\\ :L =L :S >.< }:-) }:) 3:-) 3:)";
        String[] emoticons = e.split("\\s");
        String slangWords = "lol lolz rofl lolol lmao lmsao ty tyvm omg lmao sux wtf 2fb omfg ya tbh tf yolo smh xox dont " +
                "yall til dnt ";
        String[] cusswords = "fuck shit damn bitch hoe cunt ass nigga gdit screwed screw nig".split("\\s");
        String[] keywords = "Why How Text TXT Call Donate Visit Help learn please update Donate donation volunteer pm donating".split("\\s");
        //-------------------------------------------------------------------
        int tweetcount = 0;


        for(TurkTweet t : tweets.tset){

            // Contains hashtag
            int htCnt = 0;
            if(t.tweet.contains("#")){
                Pattern htPat = Pattern.compile("\\#");
                Matcher htMatch = htPat.matcher(t.tweet);
                while(htMatch.find()){
                    htCnt++;
                }
                addAttribute("1", "containsHashTag", "{0, 1}");
            } else{
                addAttribute("0", "containsHashTag", "{0, 1}");
            }


            // Get sentence parsed tweet
            String[] sentenceParsedTweet = sentence.getSentencesFromString(t.tweet);
            int hasOneWordSentence = 0;

            // Has 1 word sentence
            for(String i : sentenceParsedTweet){
                if(i.split("\\s").length == 1){
                    hasOneWordSentence = 1;

                }
            }
            if(hasOneWordSentence == 1) addAttribute("0", "oneWordSentence", "{0, 1}");
            else addAttribute("1", "oneWordSentence", "{0, 1}");

            // Check for multiple sentences
            if(sentenceParsedTweet.length > 1){
                addAttribute("0", "multipleSentences", "{0, 1}");
            }
            else addAttribute("1", "multipleSentences", "{0, 1}");


            // URL tracking
            Pattern linkPattern = Pattern
                    .compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Pattern pnPattern = Pattern
                    .compile("[0-9]{3}");
            Pattern pnPattern2 = Pattern
                    .compile("^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-‌​9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})$");
            Matcher linkMatch = linkPattern.matcher(t.tweet);

            String urlTweet = t.tweet;
            String url;
            Pattern dateInLinkPattern = Pattern
                    .compile("[0-9]{4}/[0-9]{2}");
            String newsURL = "nyt cnn news huff post time press gov po.st abc ne.ws";

            if(urlTweet.contains("http://t.co") || urlTweet.contains("http://bit.ly") || urlTweet.contains("http://goo.gl") || urlTweet.contains("http://tinyurl.com")){
                boolean goodURL = true;
                int pos = urlTweet.indexOf("http://t.co");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://tinyurl.com");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://bit.ly");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://goog.gl");
                int pos2 = urlTweet.indexOf(" ", pos);
                if(pos2 == -1){
                    urlTweet = urlTweet + " ";
                    pos2 = urlTweet.length()-1; //end of line
                }
                url = lengthen(urlTweet.substring(pos, pos2));
                System.out.println(url);
                urlWriter.write(t.tweet + "\n" + t.classification + "\n" + url + "\n\n");
                Matcher dtMatch = dateInLinkPattern.matcher(url);
                if(dtMatch.find()) goodURL = true;
                for(String i : newsURL.split(" ")){
                    if(url.contains(i)) goodURL = true;
                }
                if(url.split("-").length > 3 || url.split("_").length > 3)
                    goodURL = true;
                String badLinks = "instagr 4sq photo twitter youtube";
                for(String i : badLinks.split(" ")){
                    if(url.contains(i)) goodURL = false;
                }


                if(goodURL)
                    addAttribute("1", "goodURL", "{0,1}");
                else addAttribute("0", "goodURL", "{0,1}");
            }else{
                addAttribute("0", "goodURL", "{0,1}");
            }

            Matcher pnMatch = pnPattern.matcher(t.tweet);
            Matcher pnMatch2 = pnPattern.matcher(t.tweet);
            if(pnMatch.find() || pnMatch2.find()){
                addAttribute("1", "containsPhoneNumber", "{0, 1}");
            } else{
                addAttribute("0", "containsPhoneNumber", "{0, 1}");
            }

            int hasEmoticon = 0;
            for(String i : emoticons){
                if(t.tweet.contains(i)){
                    hasEmoticon = 1;
                    break;
                }
            }

            // Contains emoticon
            if(hasEmoticon == 1){
                addAttribute("1", "containsEmoticon", "{0, 1}");
            } else{
                addAttribute("0", "containsEmoticon", "{0, 1}");
            }

            // Contains retweet
            if(t.tweet.contains("RT")){
                addAttribute("1", "containsRetweet", "{0, 1}");
            } else {
                addAttribute("0", "containsRetweet", "{0, 1}");
            }

            // Contains keyword
            String lowerCaseTweet = t.tweet.toLowerCase();
            String containsKeyWord = "0";
            for(String i : keywords){
                if(t.tweet.contains(i)){
                    containsKeyWord = "1";
                    break;
                }
            }
            addAttribute(containsKeyWord, "containsKeyword", "{0, 1}");

            // Contains cuss word
            String containsCuss = "0";
            for(String i : cusswords){
                if(lowerCaseTweet.contains(i)){
                    containsCuss = "1";
                    break;
                }
            }

            addAttribute(containsCuss, "containsCuss", "{0, 1}");

            // Classification
            addAttribute(t.classification, "classification", "{conversational informational}");

            // Add the end of the data thing
            if(first){
                newLine += "\n";
                writer.write(header_+ "\n@data\n");
                writer.write(newLine);
                first = false;
            } else{
                newLine += "\n";
                System.out.println(t.tweet);
                writer.write(newLine);
            }

            tweetcount++;
            System.out.println(tweetcount);
            System.out.println(newLine);
            attribute_count = 0;
            newLine = "";
        }
        writer.close();
        urlWriter.close();
    }
}
