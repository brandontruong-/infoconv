import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by brand_000 on 12/26/13.
 */
public class Sentence {
    SentenceModel model;
    SentenceDetectorME sentenceDetector;

    public Sentence() throws FileNotFoundException {
        loadSentenceModel();
        sentenceDetector = new SentenceDetectorME(model);


    }

    public void loadSentenceModel() throws FileNotFoundException{
        InputStream modelIn = new FileInputStream("models/en-sent.bin");
        try {
            model = new SentenceModel(modelIn);
            if(model != null) System.out.println("Sentence detector loaded");
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
            if(modelIn != null){
                try{
                    modelIn.close();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }
        }

    }

    public String[] getSentencesFromString(String str){
        if(sentenceDetector != null){
            String sentences[] = sentenceDetector.sentDetect(str);
            return sentences;
        }
        else {
            System.out.println("failed");
            return null;
        }
    }

    public void getSentenceCount(String str){

    }
}
