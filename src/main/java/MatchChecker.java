/**
 * Created by brand_000 on 10/30/14.
 */
public class MatchChecker {
    public void checkOldNew(Tweets turk1, Tweets turk2){
        // Check each turktweet
        // have match tweets out of turk1 to turk2
        // match variable for each question --- add 1 if match, do nothing if not match
        int q1 = 0, q2 = 0, q3 = 0, q4 = 0, q5 = 0, q6 = 0, q7 = 0, q8 = 0, q9 = 0;
        for(int i = 0; i < turk1.tset.size() - 1; i++){
            if(!turk1.tset.get(i).q1.equals(turk2.tset.get(i).q1)){
                q1++;
            }
            if(!turk1.tset.get(i).q2.equals(turk2.tset.get(i).q2)){
                q2++;
            }
            if(!turk1.tset.get(i).q3.equals(turk2.tset.get(i).q3)){
                q3++;
            }
            if(!turk1.tset.get(i).q4.equals(turk2.tset.get(i).q4)){
                q4++;
            }
            if(!turk1.tset.get(i).q5.equals(turk2.tset.get(i).q5)){
                q5++;
            }
            if(!turk1.tset.get(i).q6.equals(turk2.tset.get(i).q6)){
                q6++;
            }
            if(!turk1.tset.get(i).q7.equals(turk2.tset.get(i).q7)){
                q7++;
            }
            if(!turk1.tset.get(i).q8.equals(turk2.tset.get(i).q8)){
                q8++;
            }
            if(!turk1.tset.get(i).q9.equals(turk2.tset.get(i).q9)){
                q9++;
            }


        }
        System.out.println("q1 percentage: " + (double)q1/turk1.tset.size());
        System.out.println("q2 percentage: " + ((double)q2/turk1.tset.size()));
        System.out.println("q3 percentage: " + ((double)q3/turk1.tset.size()));
        System.out.println("q4 percentage: " + ((double)q4/turk1.tset.size()));
        System.out.println("q5 percentage: " + ((double)q5/turk1.tset.size()));
        System.out.println("q6 percentage: " + ((double)q6/turk1.tset.size()));
        System.out.println("q7 percentage: " + ((double)q7/turk1.tset.size()));
        System.out.println("q8 percentage: " + ((double)q8/turk1.tset.size()));
        System.out.println("q9 percentage: " + ((double)q9/turk1.tset.size()));

        System.out.println("q1: " + q1);
        System.out.println("q2: " + q2);
        System.out.println("q3: " + q3);
        System.out.println("q4: " + q4);
        System.out.println("q5: " + q5);
        System.out.println("q6: " + q6);
        System.out.println("q7: " + q7);
        System.out.println("q8: " + q8);
        System.out.println("q9: " + q9);

    }
}
