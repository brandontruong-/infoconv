/**
 * Created by brandon on 2/16/14.
 */
public class Tweet {
    int length;
    String id, user, time, tweet, classification, sentiment, geo;
    public Tweet(int len, String ID, String u, String tim, String t, String clas){
        t = t.replace(",", " ");

        length = len;
        id = ID.toString();
        user = u;
        time = tim;
        tweet = t.toString();
        classification = clas;
    }
}
