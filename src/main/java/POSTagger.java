import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by brandon on 2/16/14.
 */
public class POSTagger {
    POSModel model;
    POSTaggerME pos;

    public void POSTagger() throws FileNotFoundException{
        loadPOS();
        pos = new POSTaggerME(model);
    }

    public void loadPOS() throws FileNotFoundException{
        InputStream modelIn = new FileInputStream("models/en-sent.bin");
        try{
            modelIn = new FileInputStream("en-pos-maxent.bin");
            model = new POSModel(modelIn);
        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally{
            if(modelIn != null){
                try{
                    modelIn.close();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
