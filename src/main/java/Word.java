/**
 * Created by brandon on 4/19/14.
 */
public class Word implements Comparable{

    String word;
    int count = 0;

    public Word(String w){
        super();
        word = w;
        count = 1;
    }

    public void addCount(){
        count++;
    }

    @Override
    public int compareTo(Object compareWord) {
        int compareCount = ((Word) compareWord).count;
        return compareCount - this.count;
    }
}
