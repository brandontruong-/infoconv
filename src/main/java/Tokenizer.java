import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by brand_000 on 2/14/14.
 */
public class Tokenizer {
    TokenizerModel model;
    TokenizerME tokenizer;

    public void Tokenizer() throws FileNotFoundException{
        loadTokenizer();
        tokenizer = new TokenizerME(model);
    }

    public void loadTokenizer() throws FileNotFoundException{
        InputStream modelIn = new FileInputStream("models/en-token.bin");
        try{
            model = new TokenizerModel(modelIn);
            if(model != null) System.out.println("Tokenizer loaded.");
        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally {
            if(modelIn != null){
                try{
                    modelIn.close();
                }
                catch(IOException e){

                }
            }
        }
    }

    public String[] tokenize(String s){
        if(tokenizer != null){
            String tokens[] = tokenizer.tokenize(s);
            return tokens;
        }
        else{
            System.out.println("failed");
            return null;
        }
    }
}
