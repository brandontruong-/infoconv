import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collections;


/**
 * Created by brandon on 2/16/14.
 */
public class Tweets {
    ArrayList<Tweet> set = new ArrayList<Tweet>();
    ArrayList<TurkTweet> tset = new ArrayList<TurkTweet>();

    public Tweets(String filepath, String type) throws IOException {
        if (type.equals("normal")) {
            splitCSVtoTweets(filepath);
        } else if (type.equals("turk")) {
            splitCSVtoTurkTweets(filepath);
        }
    }

    public void splitCSVtoTweets(String filepath) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filepath));
        String line;
        String[] tempTweet;
        int lineNum = 0;
        //skip the first line
        line = br.readLine();
        while ((line = br.readLine()) != null) {
            int len = line.length();
            tempTweet = line.split(",");

            try {
                set.add(new Tweet(line.length(), tempTweet[0], tempTweet[1], tempTweet[2], tempTweet[3], tempTweet[5]));
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e);
            }
            lineNum++;
        }
        System.out.println("Split " + lineNum + " tweets");
        br.close();
    }

    public void splitCSVtoTurkTweets(String filepath) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filepath));
        String line;
        String[] tempTweet;
        int lineNum = 0;
        line = br.readLine();

        while ((line = br.readLine()) != null) {
            int len = line.length();
            tempTweet = line.split(",");

            // System.out.println(tempTweet[13]);//relevance
//            for(String tweet : tempTweet){
//                System.out.println(tweet);
//            }
            try {
                tset.add(new TurkTweet(tempTweet[12], tempTweet[10], tempTweet[11], tempTweet[13], tempTweet[14], tempTweet[15], tempTweet[16], tempTweet[17], tempTweet[18], tempTweet[19], tempTweet[20], tempTweet[21], "unknownGeo", line));
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e);
            }
            lineNum++;
        }
    }

    public ArrayList<TurkTweet> getNewdata(int times, String what, String pos, String neg) throws IOException {
        //Scanner newdata = new Scanner(new File())
        ArrayList<TurkTweet> tset3 = new ArrayList<TurkTweet>();
        ArrayList<TurkTweet> tset2 = tset;
        int t = 0;
        int f = 0;
//        System.out.println(tset2);
        for (TurkTweet y : tset2) {
            System.out.println(y.getAnswer(what, pos, neg));
            if (y.getAnswer(what, pos, neg) == 0) {
                t++;
            }
            if (y.getAnswer(what, pos, neg) == 1) {
                f++;
            }
        }
        System.out.println("Amount true: " + t + " Amount false: " + f);
        //System.out.println(tset2);
        int many = 0;
        Collections.shuffle(tset2);
        //System.out.println(tset2);
        for (TurkTweet e : tset2) {
            if (e.getAnswer(what, pos, neg) == 0)
                tset3.add(e);
        }

        many = tset3.size();
        for (int x = 0; x < many * times; x++) {
            if (tset2.get(x).getAnswer(what, pos, neg) == 1)
                tset3.add(tset2.get(x));
        }

        for (TurkTweet o : tset3) {
            PrintWriter output = new PrintWriter(new File("Balanced.txt"));
            output.write(o.getOriginal() + "\n");
        }

        return tset3;
    }
}