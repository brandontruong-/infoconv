import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by brand_000 on 12/26/13.
 */

/**
 *
 * Baseline: BOW Approach
 * Extractor: Regular feature set
 * Combined: BOW + Regular
 * Tweet: original tweet data structure
 * TurkTweet: new tweet data structure
 *
 */

public class Main {

    public static void main(String [] args) throws Exception {
        Runner r = new Runner();

//        r.basicRun();
//        r.check1();
//        r.checkOldNew();
        //r.turkRun();

        r.organize(2,"info","YESINFO","NOINFO");
    }

}

