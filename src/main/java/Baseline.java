import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by brandon on 4/19/14.
 */
public class Baseline {

    String newLine = "{";
    Tweets tweets;
    String header = "@relation informationVSconversation\n\n";
    boolean first = true;
    int attribute_count = 0;
    ArrayList<Word> vocabulary = new ArrayList<Word>();
    ArrayList<Word> topVocab = new ArrayList<Word>();
    public Baseline(Tweets twts, String option) throws IOException {
        tweets = twts;
        if(option == "1"){
            getVocabulary();
            extractBOWPresence();
            extractBOWReal();
        } else if(option == "2"){
            getTurkVocabulary();
            extractTurkBOWPresence();
            extractTurkBOWReal();
        }
    }

    int firsta = 0;
    public void addAttribute(String n){
        if(!n.equals("0")){
            if(firsta == 0){
                newLine += attribute_count + " " + n;
                firsta++;
                attribute_count++;
            }
            else {
                newLine += "," + attribute_count + " " + n;
                attribute_count++;
            }
        }
        else{
            attribute_count++;
        }
    }

    public void getVocabulary() throws IOException{
        Tokenizer t = new Tokenizer();
        //String[] splitTweet;
        for(Tweet i : tweets.set){

            String[] splitTweet = i.tweet.split(" ");
            for(String j : splitTweet){
                if(vocabulary.isEmpty()){
                    vocabulary.add(new Word(j));
                } else {
                    for (int n = 0; n < vocabulary.size(); n++) {
                        // if vocabulary has word j, add count, else add word
                        if(n == vocabulary.size() - 1){
                            vocabulary.add(new Word(j));
                            break;
                        }
                        if (vocabulary.get(n).word.equals(j)) {
                            vocabulary.get(vocabulary.indexOf(vocabulary.get(n))).addCount();
                            break;
                        }
                    }
                }
            }

        }
        File file = new File("vocab.txt");
        Writer writer = new BufferedWriter(new FileWriter(file));


        Collections.sort(vocabulary);

        for(int o = vocabulary.size() - 1; o > 1; o--){
            if(vocabulary.get(o).count < 2){
                vocabulary.remove(vocabulary.get(o));
            }
        }

        for(Word l : vocabulary){
            writer.write("Word #" + vocabulary.indexOf(l) + " | " + l.word + " --- count: " + l.count + "\n");
        }
    }

    public void extractBOWPresence() throws IOException{

        File file = new File("baseline.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));

        for(int i = 1; i <= vocabulary.size(); i++){
            header += "@attribute" + " word" + i + " {0, 1}\n";
        }
        header += "@attribute classification {\"conversational\" \"informational\"}\n";
        header += "\n@data\n";

        writer.write(header);

        for(Tweet t: tweets.set){
            for(Word w : vocabulary){
                if(t.tweet.contains(w.word)){
                    addAttribute("1");
                }
                else{
                    addAttribute("0");
                }
            }

            newLine += "," + attribute_count + " \"" + t.classification + "\"}";
            writer.write(newLine + "\n");

            attribute_count = 0;
            newLine = "{";
            firsta = 0;
        }
        header = "@relation informationVSconversation\n\n";
        writer.close();
    }

    public void extractBOWReal() throws IOException{
        File file = new File("bow.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));

        for(int i = 1; i <= vocabulary.size(); i++){
            header += "@attribute" + " word" + i + " numeric\n";
        }
        header += "@attribute classification {\"conversational\" \"informational\"}\n";
        header += "\n@data\n";

        writer.write(header);

        for(Tweet t: tweets.set){
            for(Word w : vocabulary){
                int wCount = t.tweet.length() - t.tweet.replace(w.word, "").length();
                addAttribute(Integer.toString(wCount));
            }

            newLine += "," + attribute_count + " \"" + t.classification + "\"}";
            writer.write(newLine + "\n");

            attribute_count = 0;
            newLine = "{";
            firsta = 0;
        }
        header = "@relation informationVSconversation\n\n";
        writer.close();
    }

    public void getTurkVocabulary() throws IOException{
        Tokenizer t = new Tokenizer();
        //String[] splitTweet;
        for(TurkTweet i : tweets.tset){

            String[] splitTweet = i.tweet.split(" ");
            for(String j : splitTweet){
                if(vocabulary.isEmpty()){
                    vocabulary.add(new Word(j));
                } else {
                    for (int n = 0; n < vocabulary.size(); n++) {
                        // if vocabulary has word j, add count, else add word
                        if(n == vocabulary.size() - 1){
                            vocabulary.add(new Word(j));
                            break;
                        }
                        if (vocabulary.get(n).word.equals(j)) {
                            vocabulary.get(vocabulary.indexOf(vocabulary.get(n))).addCount();
                            break;
                        }
                    }
                }
            }

        }
        File file = new File("Turkvocab.txt");
        Writer writer = new BufferedWriter(new FileWriter(file));


        Collections.sort(vocabulary);

        for(int o = vocabulary.size() - 1; o > 1; o--){
            if(vocabulary.get(o).count < 2){
                vocabulary.remove(vocabulary.get(o));
            }
        }

        for(Word l : vocabulary){
            writer.write("Word #" + vocabulary.indexOf(l) + " | " + l.word + " --- count: " + l.count + "\n");
        }
    }

    public void extractTurkBOWPresence() throws IOException{

        File file = new File("baselineTurk.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));

        for(int i = 1; i <= vocabulary.size(); i++){
            header += "@attribute" + " word" + i + " {0, 1}\n";
        }
        header += "@attribute classification {YesDis, NoDis, IDKDis}\n";
        header += "\n@data\n";

        writer.write(header);

        for(TurkTweet t: tweets.tset){
            for(Word w : vocabulary){
                if(t.tweet.contains(w.word)){
                    addAttribute("1");
                }
                else{
                    addAttribute("0");
                }
            }

            newLine += "," + attribute_count + " \"" + t.classification + "\"}";
            writer.write(newLine + "\n");

            attribute_count = 0;
            newLine = "{";
            firsta = 0;
        }
        header = "@relation informationVSconversation\n\n";
        writer.close();
    }

    public void extractTurkBOWReal() throws IOException{
        File file = new File("bowTurk.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));

        for(int i = 1; i <= vocabulary.size(); i++){
            header += "@attribute" + " word" + i + " numeric\n";
        }
        header += "@attribute classification {YesDis, NoDis, IDKDis}\n";
        header += "\n@data\n";

        writer.write(header);

        for(TurkTweet t: tweets.tset){
            for(Word w : vocabulary){
                int wCount = t.tweet.length() - t.tweet.replace(w.word, "").length();
                addAttribute(Integer.toString(wCount));
            }

            newLine += "," + attribute_count + " \"" + t.classification + "\"}";
            writer.write(newLine + "\n");

            attribute_count = 0;
            newLine = "{";
            firsta = 0;
        }
        header = "@relation informationVSconversation\n\n";
        writer.close();
    }
}

