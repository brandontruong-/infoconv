import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Combined {
    String newLine = "{";
    Tweets tweets;
    String header = "@relation informationVSconversation\n\n";
    int attribute_count = 0;
    ArrayList<Word> vocabulary = new ArrayList<Word>();
    ArrayList<Word> topVocab = new ArrayList<Word>();
    int firsta = 0;

    public Combined(Tweets twts) throws Exception {
        tweets = twts;
        getVocabulary();
        extract();
    }



    public void addAttribute(String n) {
        if (!n.equals("0")) {
            if (firsta == 0) {
                newLine += attribute_count + " " + n;
                firsta++;
                attribute_count++;
                return;
            } else {
                newLine += "," + attribute_count + " " + n;
                attribute_count++;
                return;
            }
        } else {
            attribute_count++;
            return;
        }
    }

    public static String lengthen(String getUrl) throws Exception{
        while(getUrl.contains("http://t.co") || getUrl.contains("http://bit.ly") || getUrl.contains("http://goo.gl") || getUrl.contains("http://tinyurl.com")){
            URL shortened = new URL(getUrl.toString());
            HttpURLConnection urlConnection = (HttpURLConnection) shortened.openConnection();
            urlConnection.setInstanceFollowRedirects(false);
            getUrl = urlConnection.getHeaderField("location");
            if(getUrl == null)
                return "invalid";
        }
        return getUrl;

    }

    public void getVocabulary() throws IOException {
        Tokenizer t = new Tokenizer();
        for (Tweet i : tweets.set) {

            String[] splitTweet = i.tweet.split(" ");
            for (String j : splitTweet) {
                if (vocabulary.isEmpty()) {
                    vocabulary.add(new Word(j));
                } else {
                    for (int n = 0; n < vocabulary.size(); n++) {
                        // if vocabulary has word j, add count, else add word
                        if (n == vocabulary.size() - 1) {
                            vocabulary.add(new Word(j));
                            break;
                        }
                        if (vocabulary.get(n).word.equals(j)) {
                            vocabulary.get(vocabulary.indexOf(vocabulary.get(n))).addCount();
                            break;
                        }
                    }
                }
            }

        }
        File file = new File("vocab");
        Writer writer = new BufferedWriter(new FileWriter(file));


        Collections.sort(vocabulary);

        for (int o = vocabulary.size() - 1; o > 1; o--) {
            if (vocabulary.get(o).count < 2) {
                vocabulary.remove(vocabulary.get(o));
            }
        }

        for (Word l : vocabulary) {
            writer.write(l.word + " --- count: " + l.count + "\n");
        }
    }

    public void extract() throws Exception {

        File file = new File("combined.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));
        File urlFile = new File("urls");
        Writer urlWriter = new BufferedWriter(new FileWriter(urlFile));
        header += "@attribute containsHashTag {0, 1}\n" +
                "@attribute oneWordSentence {0, 1}\n" +
                "@attribute multipleSentences {0, 1}\n" +
                "@attribute goodURL {0,1}\n" +
                "@attribute containsPhoneNumber {0, 1}\n" +
                "@attribute containsEmoticon {0, 1}\n" +
                "@attribute containsRetweet {0, 1}\n" +
                "@attribute containsKeyword {0, 1}\n" +
                "@attribute containsCuss {0, 1}\n";

        for (int i = 1; i <= vocabulary.size(); i++) {
            header += "@attribute" + " word" + i + " {0, 1}\n";
        }

        header += "@attribute classification {\"conversational\" \"informational\"}\n";
        header += "\n@data\n";

        writer.write(header);

        Sentence sentence = new Sentence();
        //------------------------------Dictionaries-------------------------------------

        String e = ":-) :) :o) :] :3 :c) :> =] 8) =) :} :^) :'-) :')" +
                ":-D :D 8-D 8D x-D xD X-D XD =-D =D =-3 =3 B^D :-)) |;-) |-O <3 >:[ :-( :( :-c :c :-< :< :-[ :[ :{ :'-( :'( QQ" +
                ":-|| :@ >:( D:< D: D8 D; D= DX v.v D-': >:) >;) >:-)" +
                ">:\\ >:/ :-/ :-. :/ :\\ =/ =\\ :L =L :S >.< }:-) }:) 3:-) 3:)";
        String[] emoticons = e.split("\\s");
        String slangWords = "lol lolz rofl lolol lmao lmsao ty tyvm omg lmao sux wtf 2fb omfg ya tbh tf yolo smh xox dont " +
                "yall til dnt ";
        String[] cusswords = "fuck shit damn bitch hoe cunt ass nigga gdit screwed screw".split("\\s");
        String[] keywords = "Why How Text TXT Call Donate Visit Help learn please update Donate donation volunteer pm donating".split("\\s");

        //-------------------------------------------------------------------
        // Maybe try combined version of all punctuation or different combinations thereof.

        int tweetcount = 0;
        for (Tweet t : tweets.set) {

            //-----------------Contains certain characters-------------------

            int htCnt = 0;
            if(t.tweet.contains("#")){
                Pattern htPat = Pattern.compile("\\#");
                Matcher htMatch = htPat.matcher(t.tweet);
                while(htMatch.find()){
                    htCnt++;
                }
                addAttribute("1");
            } else{
                addAttribute("0");
            }

            //------------------------Sentence Structures---------------------------------
            // Get sentence parsed tweet
            String[] sentenceParsedTweet = sentence.getSentencesFromString(t.tweet);
            int hasOneWordSentence = 0;

            //---------------------One Word Sentence------------------------------------------
            for(String i : sentenceParsedTweet){
                if(i.split("\\s").length == 1){
                    hasOneWordSentence = 1;

                }
            }
            if(hasOneWordSentence == 1) addAttribute("0");
            else addAttribute("1");

            // Check for multiple sentences
            if(sentenceParsedTweet.length > 1){
                addAttribute("0");
            }
            else addAttribute("1");


            //--------------------------------------Smart URL Finding-------------------------------------
            Pattern linkPattern = Pattern
                    .compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Pattern pnPattern = Pattern
                    .compile("[0-9]{3}");
            Pattern pnPattern2 = Pattern
                    .compile("^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-‌​9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})$");
            Matcher linkMatch = linkPattern.matcher(t.tweet);

            String urlTweet = t.tweet;
            String url;
            Pattern dateInLinkPattern = Pattern
                    .compile("[0-9]{4}/[0-9]{2}");
            String newsURL = "nyt cnn news huff post time press gov po.st abc ne.ws";

            if(urlTweet.contains("http://t.co") || urlTweet.contains("http://bit.ly") || urlTweet.contains("http://goo.gl") || urlTweet.contains("http://tinyurl.com")){
                boolean goodURL = true;
                int pos = urlTweet.indexOf("http://t.co");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://tinyurl.com");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://bit.ly");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://goog.gl");
                int pos2 = urlTweet.indexOf(" ", pos);
                if(pos2 == -1){
                    urlTweet = urlTweet + " ";
                    pos2 = urlTweet.length()-1; //end of line
                }
                url = lengthen(urlTweet.substring(pos, pos2));
                System.out.println(url);
                urlWriter.write(t.tweet + "\n" + t.classification + "\n" + url + "\n\n");
                Matcher dtMatch = dateInLinkPattern.matcher(url);
                if(dtMatch.find()) goodURL = true;
                for(String i : newsURL.split(" ")){
                    if(url.contains(i)) goodURL = true;
                }
                if(url.split("-").length > 3 || url.split("_").length > 3)
                    goodURL = true;
                String badLinks = "instagr 4sq photo twitter youtube";
                for(String i : badLinks.split(" ")){
                    if(url.contains(i)) goodURL = false;
                }


                if(goodURL)
                    addAttribute("1");
                else addAttribute("0");
            }else{
                addAttribute("0");
            }

            //------------------------------Phone Number--------------------------------------
            Matcher pnMatch = pnPattern.matcher(t.tweet);
            Matcher pnMatch2 = pnPattern.matcher(t.tweet);
            if(pnMatch.find() || pnMatch2.find()){
                addAttribute("1");
            } else{
                addAttribute("0");
            }

            //-----------------------------Emoticon--------------------------------------------
            int hasEmoticon = 0;
            for(String i : emoticons){
                if(t.tweet.contains(i)){
                    hasEmoticon = 1;
                    break;
                }
            }

            if(hasEmoticon == 1){
                addAttribute("1");
            } else{
                addAttribute("0");
            }

            //-----------------------------Retweet------------------------------------
            if(t.tweet.contains("RT")){
                addAttribute("1");
            } else {
                addAttribute("0");
            }

            //-------------------------------Keyword------------------------------------
            String lowerCaseTweet = t.tweet.toLowerCase();
            String containsKeyWord = "0";
            for(String i : keywords){
                if(t.tweet.contains(i)){
                    containsKeyWord = "1";
                    break;
                }
            }
            addAttribute(containsKeyWord);

            //------------------------------------Cuss Word--------------------------------
            String containsCuss = "0";
            for(String i : cusswords){
                if(lowerCaseTweet.contains(i)){
                    containsCuss = "1";
                    break;
                }
            }

            addAttribute(containsCuss);

            for (Word w : vocabulary) {
                if (t.tweet.contains(w.word)) {
                    addAttribute("1");
                } else {
                    addAttribute("0");
                }
            }

            newLine += "," + attribute_count + " \"" + t.classification + "\"}";
            writer.write(newLine + "\n");

            attribute_count = 0;
            newLine = "{";
            firsta = 0;
        }

        writer.close();
    }

    public void turkExtract() throws Exception {

        File file = new File("turkCombined.arff");
        Writer writer = new BufferedWriter(new FileWriter(file));
        File urlFile = new File("urls");
        Writer urlWriter = new BufferedWriter(new FileWriter(urlFile));
        header += "@attribute containsHashTag {0, 1}\n" +
                "@attribute oneWordSentence {0, 1}\n" +
                "@attribute multipleSentences {0, 1}\n" +
                "@attribute goodURL {0,1}\n" +
                "@attribute containsPhoneNumber {0, 1}\n" +
                "@attribute containsEmoticon {0, 1}\n" +
                "@attribute containsRetweet {0, 1}\n" +
                "@attribute containsKeyword {0, 1}\n" +
                "@attribute containsCuss {0, 1}\n";

        for (int i = 1; i <= vocabulary.size(); i++) {
            header += "@attribute" + " word" + i + " {0, 1}\n";
        }

        header += "@attribute classification {\"conversational\" \"informational\"}\n";
        header += "\n@data\n";

        writer.write(header);

        Sentence sentence = new Sentence();
        //------------------------------Dictionaries-------------------------------------

        String e = ":-) :) :o) :] :3 :c) :> =] 8) =) :} :^) :'-) :')" +
                ":-D :D 8-D 8D x-D xD X-D XD =-D =D =-3 =3 B^D :-)) |;-) |-O <3 >:[ :-( :( :-c :c :-< :< :-[ :[ :{ :'-( :'( QQ" +
                ":-|| :@ >:( D:< D: D8 D; D= DX v.v D-': >:) >;) >:-)" +
                ">:\\ >:/ :-/ :-. :/ :\\ =/ =\\ :L =L :S >.< }:-) }:) 3:-) 3:)";
        String[] emoticons = e.split("\\s");
        String slangWords = "lol lolz rofl lolol lmao lmsao ty tyvm omg lmao sux wtf 2fb omfg ya tbh tf yolo smh xox dont " +
                "yall til dnt ";
        String[] cusswords = "fuck shit damn bitch hoe cunt ass nigga gdit screwed screw".split("\\s");
        String[] keywords = "Why How Text TXT Call Donate Visit Help learn please update Donate donation volunteer pm donating".split("\\s");

        //-------------------------------------------------------------------
        // Maybe try combined version of all punctuation or different combinations thereof.

        int tweetcount = 0;
        for (TurkTweet t : tweets.tset) {

            //-----------------Contains certain characters-------------------

            int htCnt = 0;
            if(t.tweet.contains("#")){
                Pattern htPat = Pattern.compile("\\#");
                Matcher htMatch = htPat.matcher(t.tweet);
                while(htMatch.find()){
                    htCnt++;
                }
                addAttribute("1");
            } else{
                addAttribute("0");
            }

            //------------------------Sentence Structures---------------------------------
            // Get sentence parsed tweet
            String[] sentenceParsedTweet = sentence.getSentencesFromString(t.tweet);
            int hasOneWordSentence = 0;

            //---------------------One Word Sentence------------------------------------------
            for(String i : sentenceParsedTweet){
                if(i.split("\\s").length == 1){
                    hasOneWordSentence = 1;

                }
            }
            if(hasOneWordSentence == 1) addAttribute("0");
            else addAttribute("1");

            // Check for multiple sentences
            if(sentenceParsedTweet.length > 1){
                addAttribute("0");
            }
            else addAttribute("1");


            //--------------------------------------Smart URL Finding-------------------------------------
            Pattern linkPattern = Pattern
                    .compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Pattern pnPattern = Pattern
                    .compile("[0-9]{3}");
            Pattern pnPattern2 = Pattern
                    .compile("^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-‌​9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})$");
            Matcher linkMatch = linkPattern.matcher(t.tweet);

            String urlTweet = t.tweet;
            String url;
            Pattern dateInLinkPattern = Pattern
                    .compile("[0-9]{4}/[0-9]{2}");
            String newsURL = "nyt cnn news huff post time press gov po.st abc ne.ws";

            if(urlTweet.contains("http://t.co") || urlTweet.contains("http://bit.ly") || urlTweet.contains("http://goo.gl") || urlTweet.contains("http://tinyurl.com")){
                boolean goodURL = true;
                int pos = urlTweet.indexOf("http://t.co");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://tinyurl.com");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://bit.ly");
                if(pos == -1)
                    pos = urlTweet.indexOf("http://goog.gl");
                int pos2 = urlTweet.indexOf(" ", pos);
                if(pos2 == -1){
                    urlTweet = urlTweet + " ";
                    pos2 = urlTweet.length()-1; //end of line
                }
                url = lengthen(urlTweet.substring(pos, pos2));
                System.out.println(url);
                urlWriter.write(t.tweet + "\n" + t.classification + "\n" + url + "\n\n");
                Matcher dtMatch = dateInLinkPattern.matcher(url);
                if(dtMatch.find()) goodURL = true;
                for(String i : newsURL.split(" ")){
                    if(url.contains(i)) goodURL = true;
                }
                if(url.split("-").length > 3 || url.split("_").length > 3)
                    goodURL = true;
                String badLinks = "instagr 4sq photo twitter youtube";
                for(String i : badLinks.split(" ")){
                    if(url.contains(i)) goodURL = false;
                }


                if(goodURL)
                    addAttribute("1");
                else addAttribute("0");
            }else{
                addAttribute("0");
            }

            //------------------------------Phone Number--------------------------------------
            Matcher pnMatch = pnPattern.matcher(t.tweet);
            Matcher pnMatch2 = pnPattern.matcher(t.tweet);
            if(pnMatch.find() || pnMatch2.find()){
                addAttribute("1");
            } else{
                addAttribute("0");
            }

            //-----------------------------Emoticon--------------------------------------------
            int hasEmoticon = 0;
            for(String i : emoticons){
                if(t.tweet.contains(i)){
                    hasEmoticon = 1;
                    break;
                }
            }

            if(hasEmoticon == 1){
                addAttribute("1");
            } else{
                addAttribute("0");
            }

            //-----------------------------Retweet------------------------------------
            if(t.tweet.contains("RT")){
                addAttribute("1");
            } else {
                addAttribute("0");
            }

            //-------------------------------Keyword------------------------------------
            String lowerCaseTweet = t.tweet.toLowerCase();
            String containsKeyWord = "0";
            for(String i : keywords){
                if(t.tweet.contains(i)){
                    containsKeyWord = "1";
                    break;
                }
            }
            addAttribute(containsKeyWord);

            //------------------------------------Cuss Word--------------------------------
            String containsCuss = "0";
            for(String i : cusswords){
                if(lowerCaseTweet.contains(i)){
                    containsCuss = "1";
                    break;
                }
            }

            addAttribute(containsCuss);

            for (Word w : vocabulary) {
                if (t.tweet.contains(w.word)) {
                    addAttribute("1");
                } else {
                    addAttribute("0");
                }
            }

            newLine += "," + attribute_count + " \"" + t.classification + "\"}";
            writer.write(newLine + "\n");

            attribute_count = 0;
            newLine = "{";
            firsta = 0;
        }

        writer.close();
    }
}

