#!/usr/bin/perl

use strict;
use warnings;

# usage: ./html_symbol_entities.pl input_file > output_file

my %printable = ('&quot;'	=>	'"',
                 '&amp;'	=>	'&',
                 '&lt;'		=>	'<',
                 '&gt;'		=>	'>',
                 '&#x20;'	=>	' ',
                 '&#x21;'	=>	'!',
                 '&#x22;'	=>	'"',
                 '&#x23;'	=>	'#',
                 '&#x24;'	=>	'$',
                 '&#x25;'	=>	'%',
                 '&#x26;'	=>	'&',
                 '&#x27;'	=>	"'",
                 '&#x28;'	=>	'(',
                 '&#x29;'	=>	')',
                 '&#x2A;'	=>	'*',
                 '&#x2B;'	=>	'+',
                 '&#x2C;'	=>	',',
                 '&#x2D;'	=>	'-',
                 '&#x2E;'	=>	'.',
                 '&#x2F;'	=>	'/',
                 '&#x30;'	=>	'0',
                 '&#x31;'	=>	'1',
                 '&#x32;'	=>	'2',
                 '&#x33;'	=>	'3',
                 '&#x34;'	=>	'4',
                 '&#x35;'	=>	'5',
                 '&#x36;'	=>	'6',
                 '&#x37;'	=>	'7',
                 '&#x38;'	=>	'8',
                 '&#x39;'	=>	'9',
                 '&#x3A;'	=>	':',
                 '&#x3B;'	=>	';',
                 '&#x3C;'	=>	'<',
                 '&#x3D;'	=>	'=',
                 '&#x3E;'	=>	'>',
                 '&#x3F;'	=>	'?',
                 '&#x40;'	=>	'@',
                 '&#x41;'	=>	'A',
                 '&#x42;'	=>	'B',
                 '&#x43;'	=>	'C',
                 '&#x44;'	=>	'D',
                 '&#x45;'	=>	'E',
                 '&#x46;'	=>	'F',
                 '&#x47;'	=>	'G',
                 '&#x48;'	=>	'H',
                 '&#x49;'	=>	'I',
                 '&#x4A;'	=>	'J',
                 '&#x4B;'	=>	'K',
                 '&#x4C;'	=>	'L',
                 '&#x4D;'	=>	'M',
                 '&#x4E;'	=>	'N',
                 '&#x4F;'	=>	'O',
                 '&#x50;'	=>	'P',
                 '&#x51;'	=>	'Q',
                 '&#x52;'	=>	'R',
                 '&#x53;'	=>	'S',
                 '&#x54;'	=>	'T',
                 '&#x55;'	=>	'U',
                 '&#x56;'	=>	'V',
                 '&#x57;'	=>	'W',
                 '&#x58;'	=>	'X',
                 '&#x59;'	=>	'Y',
                 '&#x5A;'	=>	'Z',
                 '&#x5B;'	=>	'[',
                 '&#x5C;'	=>	'\\',
                 '&#x5D;'	=>	']',
                 '&#x5E;'	=>	'^',
                 '&#x5F;'	=>	'_',
                 '&#x60;'	=>	'`',
                 '&#x61;'	=>	'a',
                 '&#x62;'	=>	'b',
                 '&#x63;'	=>	'c',
                 '&#x64;'	=>	'd',
                 '&#x65;'	=>	'e',
                 '&#x66;'	=>	'f',
                 '&#x67;'	=>	'g',
                 '&#x68;'	=>	'h',
                 '&#x69;'	=>	'i',
                 '&#x6A;'	=>	'j',
                 '&#x6B;'	=>	'k',
                 '&#x6C;'	=>	'l',
                 '&#x6D;'	=>	'm',
                 '&#x6E;'	=>	'n',
                 '&#x6F;'	=>	'o',
                 '&#x70;'	=>	'p',
                 '&#x71;'	=>	'q',
                 '&#x72;'	=>	'r',
                 '&#x73;'	=>	's',
                 '&#x74;'	=>	't',
                 '&#x75;'	=>	'u',
                 '&#x76;'	=>	'v',
                 '&#x77;'	=>	'w',
                 '&#x78;'	=>	'x',
                 '&#x79;'	=>	'y',
                 '&#x7A;'	=>	'z',
                 '&#x7B;'	=>	'{',
                 '&#x7C;'	=>	'|',
                 '&#x7D;'	=>	'}',
                 '&#x7E;'	=>	'~',
                 '&#32;'	=>	' ',
                 '&#33;'	=>	'!',
                 '&#34;'	=>	'"',
                 '&#35;'	=>	'#',
                 '&#36;'	=>	'$',
                 '&#37;'	=>	'%',
                 '&#38;'	=>	'&',
                 '&#39;'	=>	"'",
                 '&#40;'	=>	'(',
                 '&#41;'	=>	')',
                 '&#42;'	=>	'*',
                 '&#43;'	=>	'+',
                 '&#44;'	=>	',',
                 '&#45;'	=>	'-',
                 '&#46;'	=>	'.',
                 '&#47;'	=>	'/',
                 '&#48;'	=>	'0',
                 '&#49;'	=>	'1',
                 '&#50;'	=>	'2',
                 '&#51;'	=>	'3',
                 '&#52;'	=>	'4',
                 '&#53;'	=>	'5',
                 '&#54;'	=>	'6',
                 '&#55;'	=>	'7',
                 '&#56;'	=>	'8',
                 '&#57;'	=>	'9',
                 '&#58;'	=>	':',
                 '&#59;'	=>	';',
                 '&#60;'	=>	'<',
                 '&#61;'	=>	'=',
                 '&#62;'	=>	'>',
                 '&#63;'	=>	'?',
                 '&#64;'	=>	'@',
                 '&#65;'	=>	'A',
                 '&#66;'	=>	'B',
                 '&#67;'	=>	'C',
                 '&#68;'	=>	'D',
                 '&#69;'	=>	'E',
                 '&#70;'	=>	'F',
                 '&#71;'	=>	'G',
                 '&#72;'	=>	'H',
                 '&#73;'	=>	'I',
                 '&#74;'	=>	'J',
                 '&#75;'	=>	'K',
                 '&#76;'	=>	'L',
                 '&#77;'	=>	'M',
                 '&#78;'	=>	'N',
                 '&#79;'	=>	'O',
                 '&#80;'	=>	'P',
                 '&#81;'	=>	'Q',
                 '&#82;'	=>	'R',
                 '&#83;'	=>	'S',
                 '&#84;'	=>	'T',
                 '&#85;'	=>	'U',
                 '&#86;'	=>	'V',
                 '&#87;'	=>	'W',
                 '&#88;'	=>	'X',
                 '&#89;'	=>	'Y',
                 '&#90;'	=>	'Z',
                 '&#91;'	=>	'[',
                 '&#92;'	=>	'\\',
                 '&#93;'	=>	']',
                 '&#94;'	=>	'^',
                 '&#95;'	=>	'_',
                 '&#96;'	=>	'`',
                 '&#97;'	=>	'a',
                 '&#98;'	=>	'b',
                 '&#99;'	=>	'c',
                 '&#100;'	=>	'd',
                 '&#101;'	=>	'e',
                 '&#102;'	=>	'f',
                 '&#103;'	=>	'g',
                 '&#104;'	=>	'h',
                 '&#105;'	=>	'i',
                 '&#106;'	=>	'j',
                 '&#107;'	=>	'k',
                 '&#108;'	=>	'l',
                 '&#109;'	=>	'm',
                 '&#110;'	=>	'n',
                 '&#111;'	=>	'o',
                 '&#112;'	=>	'p',
                 '&#113;'	=>	'q',
                 '&#114;'	=>	'r',
                 '&#115;'	=>	's',
                 '&#116;'	=>	't',
                 '&#117;'	=>	'u',
                 '&#118;'	=>	'v',
                 '&#119;'	=>	'w',
                 '&#120;'	=>	'x',
                 '&#121;'	=>	'y',
                 '&#122;'	=>	'z',
                 '&#123;'	=>	'{',
                 '&#124;'	=>	'|',
                 '&#125;'	=>	'}',
                 '&#126;'	=>	'~');

open IN, '<', $ARGV[0] or die "Couldn't open $ARGV[0]: $!";
while (<IN>) {
  if (/&[^ ;&]+;/) {
    while (/&[^ ;&]+;/) {
      if (defined $printable{$&}) {
        s/$&/$printable{$&}/g;
      }
      else {
        last;
      }
    }
    print unless (/&[^ ;;&]+;/);
  }
  else {
    print;
  }
}
close IN;
