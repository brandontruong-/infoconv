###########################################
# Bash file for cleaning the twitter data 
###########################################

# This bash file includes several steps to clean  
# twitter data using mainly command line.
# This bash file also includes other external
# bash and perl script that need to be included
# in the working directory (spellchecker.bash and
# html_symbol_entities.pl)

# There are some points that need attention, please look for *** into the text

# 1. Taking only tweets with printable ascii characters
# This line was commented because we first separated the big twitter file into smaller parts and later we clean each one separately. This script was designed to clean mainly the emoticon data, however it can be adapted very easy to the other data.


#rm emoticon*.*

# 2. Taking only tweets with printable ascii characters
# Here can also use the following grep command
# grep -Pv '[^ -~]' positive_training.txt > emoticon_printable_ascii_only.txt 

# awk '$0 !~/[^ -~]/' negative_training.txt > emoticon_printable_ascii_only.txt

awk '$0 !~/[^ -~]/' "$1" > emoticon_printable_ascii_only.txt


# 3. Convert http commands to ascii words and remove the tweet with non printable ascii words
./awk_html_symbol_entities.pl emoticon_printable_ascii_only.txt > emoticon_html_printable.txt

# 4. Remove email from tweet  ***
# This step is required before removing the username otherwise we will get some replacement as
# 'geo.dash@yahoo.com' by  'geo.dash' so we are keeping the username of the person anyway from the 
# email
sed 's/[^ ]*[^ ]@[^ ][^ ]*/ /g' emoticon_html_printable.txt > emoticon_html_printable_noemail.txt


# 5.Remove user name 
sed 's/@[^ ][^ ]*/ /g' emoticon_html_printable_noemail.txt > emoticon_html_printable_nousername.txt


# 6. Remove the URLs from tweets and also the http word alone
sed 's/http[^ ][^ ]*/ /gI' emoticon_html_printable_nousername.txt > emoticon_html_printable_nourl.txt
#rm emoticon_html_printable_nousername.txt
sed 's/ http/ /gI' emoticon_html_printable_nourl.txt > emoticon_html_printable_nohttp.txt



# 7. Remove hashtag  
# *** Since hashtag were introduced to mark keywords or topic in a # tweets we may be removing useful information. 

sed 's/#[^ ][^ ]*/ /g' emoticon_html_printable_nohttp.txt > emoticon_html_printable_6nohash.txt


# 8. Remove tweets with only punctuation
# we can use also
# grep '[a-zA-Z]' input > output
awk '$0 ~/[a-zA-Z]/ {print $0}' emoticon_html_printable_6nohash.txt > emoticon_onesmilefinal.txt



#10. Remove RT, i.e. Retweet, from our data 
#  We can also use sed 's/\bRT\b//g' 
sed 's/\<\(RT\)\+\>/ /g' emoticon_onesmilefinal.txt | sort | uniq > emoticon_removedRT.txt


#11. Remove number from the data
# *** Here I only remove number I will remove punctuations later to avoid interference with emoticons symbols
sed 's/[0-9]/ /g' emoticon_removedRT.txt > emoticon_noNumber.txt 


# 10. Again remove tweets only with punctuation
awk '$0 ~/[a-zA-Z]/ {print $0}' emoticon_noNumber.txt  > emoticon_noemoticontweets.txt

# 11.  Removing leading spaces and spaces at the end of lines                                                                                                                                          
sed 's/^ *//'  emoticon_noemoticontweets.txt  > emoticon_NE_1.txt
sed 's/ *$//'   emoticon_NE_1.txt  > emoticon_NE_2.txt



# 15.  Remove any punctuation from tweets
sed 's/[[:punct:]]/ /g' emoticon_NE_2.txt >  emoticon_positive_NoPun.txt


#sed "s/[^[:alnum:][:space:]']/ /g" emoticon_NE_2.txt >  emoticon_NoPun_1.txt
#tr -s "'" "'" < emoticon_NoPun_1.txt > emoticon_NoPun_2.txt
#sed "s/[ ]'/ /g" emoticon_NoPun_2.txt | sed "s/'[ ]/ /g" > emoticon_NoPun_final.txt


# 16. Again remove tweets only with punctuation
awk '$0 ~/[a-zA-Z]/ {print $0}' emoticon_positive_NoPun.txt  > emoticon_positive_NE.txt


# 17. Removing duplicated tweets 
 sort emoticon_positive_NE.txt | uniq  >  emoticon_positive_NE_ND.txt
# mv emoticon_positive_NE_ND.txt emoticon_positive_NE.txt

dest="$1"
## get file name i.e. basename such as mysql.tgz
tempfile="${dest##*/}"

## display filename
#echo "${tempfile%.*}"


mv emoticon_positive_NE_ND.txt "${tempfile%.*}"_final.txt
rm emoticon*.*